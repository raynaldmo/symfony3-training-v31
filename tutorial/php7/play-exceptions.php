<?php

try {
    undefinedFunction();
    // $result = 1/0;
    // Throwable will catch Exceptions and Errors
} catch (\Throwable $error) {
    echo 'Now if you write bad code you can catch it! ' . $error->getMessage();
}


echo "\nContinue processing file";
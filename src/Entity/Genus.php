<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\GenusRepository;
use Doctrine\Common\Collections\ArrayCollection;
// use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass="App\Repository\GenusRepository")
 * @ORM\Table(name="genus")
 */
class Genus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\SubFamily")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subFamily;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Negative species! Come on...")
     * @ORM\Column(type="integer")
     */
    private $speciesCount;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $funFact;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished = true;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="date")
     */
    private $firstDiscoveredAt;

    /**
     * @ORM\OneToMany(targetEntity="GenusNote", mappedBy="genus")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $notes;

    /**
     * @ORM\OneToMany(
     *     targetEntity="GenusScientist",
     *     mappedBy="genus",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     * )
     * @Assert\Valid()
     */
    private $genusScientists;

    /**
     * Genus constructor.
     */
    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->genusScientists = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \App\Entity\SubFamily|null
     */
    public function getSubFamily(): ?SubFamily
    {
        return $this->subFamily;
    }

    public function setSubFamily(SubFamily $subFamily = null): void
    {
        $this->subFamily = $subFamily;
    }

    /**
     * @return int|null
     */
    public function getSpeciesCount(): ?int
    {
        return $this->speciesCount;
    }

    /**
     * @param int $speciesCount
     */
    public function setSpeciesCount(int $speciesCount): void
    {
        $this->speciesCount = $speciesCount;
    }

    /**
     * @return null|string
     */
    public function getFunFact(): ?string
    {
        return $this->funFact;
    }

    /**
     * @param null|string $funFact
     */
    public function setFunFact(?string $funFact): void
    {
        $this->funFact = $funFact;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return new \DateTime('-'.rand(0, 100).' days');
    }

    /**
     * @param bool $isPublished
     */
    public function setIsPublished(bool $isPublished): void
    {
        $this->isPublished = $isPublished;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFirstDiscoveredAt(): ?\DateTimeInterface
    {
        return $this->firstDiscoveredAt;
    }

    /**
     * @param \DateTime|null $firstDiscoveredAt
     */
    public function setFirstDiscoveredAt(\DateTime $firstDiscoveredAt = null) : void
    {
        $this->firstDiscoveredAt = $firstDiscoveredAt;
    }

    /**
     * @return null|string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }


    /**
     * @param $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @param \App\Entity\GenusScientist $genusScientist
     */
    public function addGenusScientist(GenusScientist $genusScientist): void
    {
        if ($this->genusScientists->contains($genusScientist)) {
            return;
        }

        $this->genusScientists[] = $genusScientist;
        // needed to update the owning side of the relationship!
        $genusScientist->setGenus($this);
    }

    /**
     * @param \App\Entity\GenusScientist $genusScientist
     */
    public function removeGenusScientist(GenusScientist $genusScientist): void
    {
        if (!$this->genusScientists->contains($genusScientist)) {
            return;
        }

        $this->genusScientists->removeElement($genusScientist);
        // needed to update the owning side of the relationship!
        $genusScientist->setGenus(null);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenusScientists(): Collection
    {
        return $this->genusScientists;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpertScientists(): Collection
    {
        return $this->getGenusScientists()->matching(
            GenusRepository::createExpertCriteria()
        );
    }
}

<?php


namespace App\EventSubscriber;

use App\Service\MessageManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;


class AddHeaderEventSubscriber implements EventSubscriberInterface
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Service\MessageManager
     */

    private $messageManager;
    /**
     * @var
     */
    private $showDiscouragingMessage;


    public function __construct(LoggerInterface $logger, MessageManager $messageManager,
        $showDiscouragingMessage)
    {
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->showDiscouragingMessage = $showDiscouragingMessage;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {

        $this->logger->info('Adding Genus application header.');

        $message = $this->showDiscouragingMessage ?
            $this->messageManager->getDiscouragingMessage() :
            $this->messageManager->getEncouragingMessage();

        $event->getResponse()
            ->headers->set(
                'X-Genus-Message',
              $message
            );
    }

    public static function getSubscribedEvents()
    {
        return [
          KernelEvents::RESPONSE => 'onKernelResponse'
        ];
    }

}